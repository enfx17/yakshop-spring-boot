package Agency04.The.YakShop.controllers;

import Agency04.The.YakShop.models.Herd;
import Agency04.The.YakShop.models.Order;
import Agency04.The.YakShop.models.Stock;
import Agency04.The.YakShop.services.YakService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/yak-shop")
@RestController
@Validated
public class YakShopController {

    private final YakService yakService;

    @Autowired
    public YakShopController(YakService yakService) {
        this.yakService = yakService;
    }

    @GetMapping(value = "/herd/{days}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Herd getHerd(@PathVariable @Min(0) int days) {
        return this.yakService.getHerdInDays(days);
    }

    @GetMapping("/stock/{days}")
    public Stock getStock(@PathVariable @Min(0) int days) {
        return yakService.getStock(days);
    }

    @PostMapping("/order/{days}")
    public ResponseEntity<Stock> order(@PathVariable @Min(0) int days, @RequestBody @Valid Order order) {
        Stock stock = yakService.serveOrder(days, order);

        if(stock.milk == null && stock.skins == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        if(stock.milk == null || stock.skins == null) {
            return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT).body(stock);
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(stock);
    }
}
