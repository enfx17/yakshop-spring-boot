package Agency04.The.YakShop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheYakShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheYakShopApplication.class, args);
	}

}
