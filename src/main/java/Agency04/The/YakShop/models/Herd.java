package Agency04.The.YakShop.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class Herd {
    @JacksonXmlProperty(localName = "labyak")
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("herd")
    public List<Labyak> labyakList;
}
