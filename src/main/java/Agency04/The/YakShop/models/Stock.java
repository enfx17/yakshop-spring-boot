package Agency04.The.YakShop.models;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.Min;

public class Stock {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @Min(0)
    public Double milk;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @Min(0)
    public Integer skins;

//    public Double getMilk() {
//        return milk;
//    }
//
//    public Integer getSkins() {
//        return skins;
//    }

//    public void setMilk(Double milk) {
//        this.milk = milk;
//    }
//
//    public void setSkins(Integer skins) {
//        this.skins = skins;
//    }
}
