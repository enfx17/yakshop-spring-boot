package Agency04.The.YakShop.models;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class Order {

    private final String customer;

    public String getCustomer() {
        return customer;
    }

    @Valid
    @NotNull
    private final Stock order;

    public Stock getOrder() {
        return order;
    }

    public Order(String customer, Stock order) {
        this.customer = customer;
        this.order = order;
    }
}
