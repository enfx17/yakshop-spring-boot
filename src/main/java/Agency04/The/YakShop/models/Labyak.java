package Agency04.The.YakShop.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Labyak {

    @JacksonXmlProperty(isAttribute = true)
    private char sex;

    @JacksonXmlProperty(isAttribute = true)
    private String name;

    @JacksonXmlProperty(isAttribute = true)
    private double age;

    private double ageLastShaved;

    private double milk = 0;

    private int skins = 0;

    @JsonIgnore
    public char getSex() {
        return sex;
    }

    public double getAge() {
        return age;
    }

    @JsonProperty("age-last-shaved")
    public double getAgeLastShaved() {
        return ageLastShaved;
    }

    @JsonIgnore
    public int getSkins() {
        return skins;
    }

    @JsonIgnore
    public double getMilk() {
        return milk;
    }

    public void setAge(double age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    @JsonIgnore
    public boolean isAlive() {
       return this.age < 10;
    }

    public void simulateTime(int days) {
        for (var i = 0; i < days; i++) {
            var ageInDays = (this.age * 100) + i;
            if (ageInDays/100 >= 10) {
                return;
            }
            produceMilk(ageInDays);
            produceSkins(ageInDays);
        }

        this.age += (double) days/100;
    }

    private void produceMilk(double ageInDays) {
        this.milk += 50 - ageInDays*0.03;
    }

    private void produceSkins(double ageInDays) {
        if ((ageInDays/100) >= 1 && (this.ageLastShaved*100 + 8 + ageInDays*0.01) <= ageInDays) {
            this.ageLastShaved = ageInDays / 100;
            this.skins++;
        }
    }

//    public double produceMilk(int days) {
//        double milk = 0;
//        for (var i = 0; i < days; i++) {
//            var ageInDays = (this.age * 100) + i;
//            if (ageInDays/100 >= 10) {
//                return milk;
//            }
//            milk += 50 - ageInDays*0.03;
//        }
//        return milk;
//    }
//
//    public int getSkins(int days) {
//        int skins = 0;
//        double timeToSkin = 0;
//        for (var i = 0; i < days; i++) {
//            var ageInDays = (this.age * 100) + i;
//            if (ageInDays/100 >= 10) {
//                return skins;
//            }
//            if ((ageInDays/100) >= 1 && timeToSkin <= 0) {
//                timeToSkin = 8 + ageInDays* 0.01;
//                skins++;
//            } else {
//                timeToSkin--;
//            }
//        }
//        return skins;
//    }
}
