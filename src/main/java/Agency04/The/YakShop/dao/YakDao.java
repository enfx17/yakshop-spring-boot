package Agency04.The.YakShop.dao;

import Agency04.The.YakShop.models.Herd;
import Agency04.The.YakShop.models.Labyak;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.List;

public interface YakDao {

    Herd getHerd();
}
