package Agency04.The.YakShop.dao;

import Agency04.The.YakShop.models.Herd;
import Agency04.The.YakShop.models.Labyak;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("MockYakRepo")
public class MockYakRepository implements YakDao {

    @Override
    public Herd getHerd() {
//        herd.add(new Labyak("Betty-1", 4));
//        herd.add(new Labyak("Betty-2", 8));
//        herd.add(new Labyak("Betty-3", 9.5));

        return new Herd();
    }
}
