package Agency04.The.YakShop.dao;

import Agency04.The.YakShop.models.Herd;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.InputStream;

@Repository("XmlYakRepo")
public class XmlYakRepository implements YakDao {

    Logger logger = LoggerFactory.getLogger(XmlYakRepository.class);

    @Override
    public Herd getHerd()  {
        return this.readHeardXML();
    }

    private Herd readHeardXML() {
        try {
            InputStream xmlFile = XmlYakRepository.class.getClassLoader().getResourceAsStream("herd.xml");
            XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
            XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(xmlFile);

            XmlMapper mapper = new XmlMapper();
            return mapper.readValue(xmlStreamReader, Herd.class);

        } catch (XMLStreamException | IOException exception) {
            logger.error(exception.getMessage());
            return null;
        }
    }

}
