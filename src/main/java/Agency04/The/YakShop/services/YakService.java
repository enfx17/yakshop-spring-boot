package Agency04.The.YakShop.services;

import Agency04.The.YakShop.dao.YakDao;
import Agency04.The.YakShop.models.Herd;
import Agency04.The.YakShop.models.Labyak;
import Agency04.The.YakShop.models.Order;
import Agency04.The.YakShop.models.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class YakService {

    private final YakDao yakDao;

    @Autowired
    public YakService(@Qualifier("XmlYakRepo") YakDao yakDao) {
        this.yakDao = yakDao;
    }

    public Herd getHerdInDays(int days) {
        Herd herd = yakDao.getHerd();

        herd.labyakList = herd.labyakList.stream().map((Labyak labyak) -> {
            labyak.simulateTime(days);
            return labyak;
        }).filter(Labyak::isAlive).collect(Collectors.toList());

        return herd;
    }

    public Stock getStock(int days) {
        Herd herd = this.yakDao.getHerd();
        Stock stock =  new Stock();
        stock.skins = 0;
        stock.milk = 0.0;

        herd.labyakList.forEach((Labyak labyak) -> {
            labyak.simulateTime(days);
            stock.milk = stock.milk + labyak.getMilk();
            stock.skins = stock.skins + labyak.getSkins();
        });

        return stock;
    }

    public Stock serveOrder(int days, Order order) {
        Stock currentStock = this.getStock(days);
        Stock orderedStock = order.getOrder();
        Stock responseStock = new Stock();

        if (orderedStock.milk != null) {
            responseStock.milk = currentStock.milk >= orderedStock.milk ? orderedStock.milk : null;
        }
        if (orderedStock.skins != null) {
            responseStock.skins = currentStock.skins >= orderedStock.skins ? orderedStock.skins : null;
        }

        return responseStock;
    }
}
